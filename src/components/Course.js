import Card from 'react-bootstrap/Card';
import Button from "react-bootstrap/Button";
const Course = (props)=>{
	let course = props.x;
	let course1 = props.word;
	return(
		<Card>
			<Card.Body>
				<Card.Title>
					<h4>{course.name} {course1}</h4>
					<h5>Description:</h5>
				</Card.Title>
				<Card.Text>
					<p>
						{course.description}
					</p>
					<h6>Price:</h6>
					<p>{course.price}</p>
				</Card.Text>
				<Button variant="primary" href="#enroll">Enroll</Button>
			</Card.Body>
		</Card>
	);
}

export default Course;
