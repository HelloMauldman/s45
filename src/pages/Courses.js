import Container from 'react-bootstrap/Container';
import Course from "../components/Course"

//Data imports
import courses from "../mock-data/courses"

const Courses = ()=>{
	const CourseCards = courses.map((course)=>{
		let x = "helloo"
		return (
			<Course x={course} word={x}/>
		)
	})

	return(
		<Container fluid>
			{CourseCards}
		</Container>
	);
};

export default Courses